<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AuthController extends ResponseController
{
    public function recaptcha(Request $request)
    {
        $client = new Client();
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => '6Le_vdgUAAAAAH_7Dh0hilJ3X-5WukiWelDgtRex',
                'response' => $request->tkcaptcha
            ]
        ]);

        $hasil = json_encode($response->getBody()->getContents());

        return $hasil;

    }

    //create user
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|',
            'email' => 'required|string|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());       
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if($user){
            $success['token'] =  $user->createToken('token')->accessToken;
            $success['message'] = "Registration successfull..";
            return $this->sendResponse($success);
        }
        else{
            $error = "Sorry! Registration is not successfull.";
            return $this->sendError($error, 401); 
        }
        
    }
    
    //login
    public function login(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|string|email',
        //     'password' => 'required'
        // ]);

        // if($validator->fails()){
        //     return $this->sendError($validator->errors());       
        // }

        // $credentials = request(['email', 'password']);
        // if(!Auth::attempt($credentials)){
        //     $error = "Unauthorized";
        //     return $this->sendError($error, 401);
        // }
        // $user = $request->user();
        // $success['token'] =  $user->createToken('token')->accessToken;
        // $success['token_type'] = "bearer";
        // return $this->sendResponse($success);

    	try {
            if(Auth::attempt(['email' =>request('email') ,'password' =>request('password')])){
                $users = Auth::user();
                $data['token'] = $users->createToken('App')->accessToken;
                return response()->json(['is_success' => 'success', 'data' => $data, 'users' => $users], 200);

            }else{
            return response()->json(['is_success' => 'fail', 'message' => 'Data yang Dimasukan Salah'], 401);
            }
        } catch(\Exception $e) {
            return response()->json(['is_success' => 'fail', 'message' => 'Data yang Dimasukan Salah'], 401);
        }
        
    }

    //logout
    public function logout(Request $request)
    {
        
        $isUser = $request->user()->token()->revoke();
        if($isUser){
            $success['message'] = "Successfully logged out.";
            return $this->sendResponse($success);
        }
        else{
            $error = "Something went wrong.";
            return $this->sendResponse($error);
        }
            
        
    }

    //getuser
    public function getUser(Request $request)
    {
        //$id = $request->user()->id;
        $user = $request->user();
        if($user){
            return $this->sendResponse($user);
        }
        else{
            $error = "user not found";
            return $this->sendResponse($error);
        }
    }
}